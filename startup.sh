#!/bin/bash
existing=$(docker container ls --filter name=fuseki -q)
if [ ! -z "$existing" ]; then
    echo Fuseki is already running.
    exit 0;
fi

stopped=$(docker container ls -a --filter name=fuseki -q)
if [ -z "$stopped" ]; then
    #No prior container created. Run fro scratch.
    docker run -d --name fuseki -p 3030:3030 -v /tmp/fuseki-data:/fuseki -e ADMIN_PASSWORD=spdxr0x stain/jena-fuseki
else
    docker start "$stopped"
fi

