#!/bin/bash

#Download the relevant scripts from Fuseki (without downloading the entire release package)
if [ ! -d bin ]; then
    mkdir bin
    cd bin
    wget https://raw.githubusercontent.com/apache/jena/master/jena-fuseki2/apache-jena-fuseki/bin/s-get
    wget https://raw.githubusercontent.com/apache/jena/master/jena-fuseki2/apache-jena-fuseki/bin/s-post
    wget https://raw.githubusercontent.com/apache/jena/master/jena-fuseki2/apache-jena-fuseki/bin/soh
    wget https://raw.githubusercontent.com/apache/jena/master/jena-fuseki2/apache-jena-fuseki/bin/s-query
    wget https://raw.githubusercontent.com/apache/jena/master/jena-fuseki2/apache-jena-fuseki/bin/s-update
    chmod +x *
fi

export PATH=$(pwd)/bin:$PATH
