#!/bin/bash
FUSEKI_CONTAINER=$(docker ps --filter name=fuseki -q)
if [ -z "$FUSEKI_CONTAINER" ]; then
    echo "I don't think it's running, boss!" 1>&2
else
    docker rm -f "$FUSEKI_CONTAINER" 1>/dev/null
fi


